# -*- coding: utf-8 -*-
from supporter import Config, Worker
import concurrent.futures as cf
from connector import Connector
from crawler import Crawler
from indexer import Indexer
import logging


config = Config()
logger = logging.getLogger(__name__)
logging.basicConfig(format=config.all().format, level=logging.DEBUG)


class Manager:

    def __init__(self):
        self.name = config.proc().name
        self.connector = Connector()
        self.cr_threads = config.proc().crawlers
        self.ix_threads = config.proc().indexers
        self.cr_urls = self.connector.get_urls()
        self.ix_urls = self.connector.get_pages()
        self.ix_names = self.connector.get_names()
        self.cr = [Crawler('{}_{}'.format(self.name, x))
                   for x in range(self.cr_threads)]
        self.ix = [Indexer(self.ix_names, '{}_{}'.format(self.name, x))
                   for x in range(self.ix_threads)]
        self.threads = self.cr_threads + self.ix_threads
        self.fs = list()
        self.exs = list()
        self.start()

    def start(self):
        with cf.ProcessPoolExecutor(max_workers=self.threads) as executor:
            self.executor = executor
            while self.cr_urls or self.ix_urls or self.fs:
                self.starter(self.cr, self.cr_urls)
                self.starter(self.ix, self.ix_urls)
                if self.fs:
                    future = next(cf.as_completed(self.fs))
                    ex = [x.worker for x in self.exs if x.future == future][0]
                    self.fs.remove(future)
                    self.exs.remove((ex, future))
                    if ex == 'Crawler':
                        self.cr.append(ex)
                        self.connector.put_urls(future.result())
                    if ex == 'Indexer':
                        self.ix.append(ex)
                        self.connector.put_names(future.result())

    def starter(self, workers, works):
        while workers and works:
            wr = workers.pop()
            future = self.executor.submit(wr.manage_url, works.pop())
            self.fs.append(future)
            self.exs.append(Worker(wr, future))
            logger.info('Process started: {}'.format(wr))


def main():
    manager = Manager()


if __name__ == '__main__':
    main()
