# -*- coding: utf-8 -*-
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from collections import namedtuple
from supporter import BaseWorker
import db
import datetime
import logging


logger = logging.getLogger(__name__)


class Connector(BaseWorker):
    """
    """
    def __init__(self, names=[], *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        logger.info("{} ready to destroy!".format(self))
        # engine = create_engine('sqlite:///sqlalchemy_example.db', echo=True)
        # Base.metadata.create_all(engine)
        # self.session = sessionmaker(bind=engine)()

    def put_urls(self, pages):
        pass
        # print(pages)
        # for page in pages:
        #     pass
        # return Result(self.name, self.result, False)

    def put_names(self, names):
        print(names)

    def get_urls(self):
        return ['https://regnum.ru/robots.txt',
                'https://regnum.ru/sitemap.xml']

    def get_pages(self):
        return ['https://regnum.ru/news/polit/2158418.html',
                'https://regnum.ru/news/polit/2159069.html']

    def get_names(self):
        return ['Путин', 'Эрдоган']
