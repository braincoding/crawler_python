# -*- coding: utf-8 -*-
"""Краулер.
Задача краулера - по указанному url ссылке найти остальные url.
"""
from urllib.parse import urlunsplit, urlsplit
from collections import namedtuple
from supporter import BaseWorker
import urllib.robotparser
import requests
import logging
import gzip
import re


logger = logging.getLogger(__name__)


class Crawler(BaseWorker):
    """Краулер.
    Задача краулера - по полученной ссылке найти другие ссылки.
    """

    def __init__(self, names=[], *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.site = None
        self.re_exp = re.compile("(http[\w.:#$%^/-]+)")
        logger.info("{} ready to destroy!".format(self))

    def manage_url(self, url):
        """
        """
        self.result = set()
        if url:
            self.url = url
            logger.info("Url received: {}".format(self.url))
            if self.site != urlsplit(self.url)[1]:
                self.site = urlsplit(self.url)[1]
                self.get_rules()
            self.process_url()
        return self.result

    def get_rules(self):
        """Метод для получения правил для указанного url

        Аргументы:
            Нет

        Возвращаемые значения:
            Нет
        """
        self.rp = urllib.robotparser.RobotFileParser()
        self.rp.set_url(self.url)
        self.rp.read()
        logger.info("Rules from {} received".format(self.url))

    def process_url(self):
        """Функция для обработки файлов.

        Аргументы:
            Нет

        Возвращаемые значения:
            Множество(set) url, полученных из по self.url
        """
        try:
            text = ''
            r = requests.get(self.url, stream=True)
            if r.status_code != 200:
                raise NameError(r.reason)
            text = r.text
            if self.url.endswith('.gz'):
                decompressedFile = gzip.GzipFile(fileobj=r.raw, mode='rb')
                text = decompressedFile.read()
                text = text.decode("UTF-8")
            self.result = set(self.re_exp.findall(text))
        except (requests.exceptions.RequestException, re.error,
                NameError, OSerror) as e:
            logger.error("File: {}, Error: {}".format(self.url, e))
