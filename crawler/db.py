# coding: utf-8
from sqlalchemy import Column, DateTime, Integer, String, Table
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Keyword(Base):
    __tablename__ = 'keywords'

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)
    person_id = Column(Integer, nullable=False)


class Page(Base):
    __tablename__ = 'pages'

    id = Column(Integer, primary_key=True)
    url = Column(String(512), nullable=False)
    site_id = Column(Integer, nullable=False)
    found_date_time = Column(DateTime, nullable=False)
    last_scan_date = Column(DateTime, nullable=False)


t_person_page_rank = Table(
    'person_page_rank', metadata,
    Column('person_id', Integer, nullable=False),
    Column('page_id', Integer, nullable=False),
    Column('rank', Integer, nullable=False)
)


class Person(Base):
    __tablename__ = 'persons'

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False, unique=True)


class Site(Base):
    __tablename__ = 'sites'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
