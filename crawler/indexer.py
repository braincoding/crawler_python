# -*- coding: utf-8 -*-
from supporter import BaseWorker
from bs4 import BeautifulSoup
import requests
import logging
import re


logger = logging.getLogger(__name__)


class Indexer(BaseWorker):

    def __init__(self, names=[], *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.names = names
        logger.info("{} ready to destroy!".format(self))

    def manage_url(self, url):
        self.result = dict()
        if url:
            self.url = url
            logger.info("{}; Url received: {}".format(self, self.url))
            try:
                r = requests.get(self.url, stream=True)
                if r.status_code != 200:
                    raise NameError(r.reason)
                soup = BeautifulSoup(r.text, 'html.parser')
                texts = ' '.join(x.text for x in soup.find_all('p'))
                for name in self.names:
                    self.result[name] = len(re.findall(name, texts))
            except (requests.exceptions.RequestException, NameError,
                    HTMLParser.HTMLParseError) as e:
                logger.error("File: {}, Error: {}".format(self.url, e))
            finally:
                return(self.result)
