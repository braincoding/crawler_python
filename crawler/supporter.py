# -*- coding: utf-8 -*-
from sqlalchemy import DateTime, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from configparser import ConfigParser
from collections import namedtuple


Base = declarative_base()
Worker = namedtuple('Worker', 'worker future')
Config_all = namedtuple('Config', 'format')
Config_Proc = namedtuple('Config_Proc', 'name crawlers indexers connectors')
Config_DB = namedtuple('Config_DB', 'user password host db')


class Config:

    def __init__(self):
        CONFIG = '../config.ini'
        DB_CONFG = '../db_config.ini'
        config = ConfigParser(interpolation=None)
        config.read(CONFIG)
        self.config = Config_all(config['PYTHON']['format'])
        self.config_proc = Config_Proc(config['PROCESSES']['name'],
                                       int(config['PROCESSES']['crawlers']),
                                       int(config['PROCESSES']['indexers']),
                                       int(config['PROCESSES']['connectors']))

        config = ConfigParser(interpolation=None)
        config.read(DB_CONFG)
        self.config_db = Config_DB(config['MYSQL']['user'],
                                   config['MYSQL']['password'],
                                   config['MYSQL']['host'],
                                   config['MYSQL']['db'])

    def all(self):
        return self.config

    def proc(self):
        return self.config_proc

    def db(self):
        return self.config_db


class BaseWorker:

    def __init__(self, name='QuickStat'):
        self.name = name

    def __repr__(self):
        return "{}: {}".format(self.__class__.__name__, self.name)

    def __eq__(self, other):
        if other == self.__class__.__name__:
            return True
        return False
